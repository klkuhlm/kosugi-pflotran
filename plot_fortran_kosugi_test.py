import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

d3 = np.loadtxt('3-parameter.out',skiprows=2)

# cols: liquid_sat, cap_press, liq_sat_est, dsat_dpres, rel_perm_liq, rel_perm_gas, dkr_dsat_liq, dkr_dsat_gas

fig = plt.figure(1,figsize=(12,5.5))
ax1 = fig.add_subplot(121)

Sr = 0.153
Srg = 1.0E-3

ax1.plot(d3[:,0],d3[:,1],'k-')
ax1.set_xlabel('liquid saturation')
ax1.set_ylabel('capillary pressure')
ax1.set_xlim([Sr,1.0])
ax1.grid()
ax1a = ax1.twinx()
ax1a.plot(d3[:,0],d3[:,3],'k--')
ax1a.set_ylabel('$\\partial$ sat/$\\partial$ pressure')
ax1a.set_xlim([Sr,1.0])

ax2 = fig.add_subplot(122)
ax2.plot(d3[:,0],d3[:,4],'r-')
ax2.plot(d3[:,0],d3[:,5],'b-')
ax2.set_xlabel('liquid saturation',color='red')
ax2.set_ylabel('relative permeability')
ax2.set_xlim([Sr,1.0])
ax2a = ax2.twinx()
ax2a.plot(d3[:,0],d3[:,6],'r--')
ax2a.plot(d3[:,0],d3[:,7],'b--')
ax2a.set_ylabel('$\\partial k_r$/$\\partial$ sat')
ax2a.set_xlim([Sr,1.0])
ax2b = ax2.twiny()
ax2b.set_xlim([1.0,Srg])
ax2b.set_xlabel('gas saturation',color='blue')
ax2.grid()

plt.tight_layout()
plt.savefig('plot_3param.png')

