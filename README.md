This repository is material associated with the development and checking of Kosugi model for PFLOTRAN. It is now included in the main PFLOTRAN repository. 

https://bitbucket.org/pflotran/pflotran/wiki/Home

This repo has a python version used for comparison, and the initial fortran version.  

The PFLOTRAN version (called "MODIFIED_KOSUGI") is documented at the following sites:

https://www.documentation.pflotran.org/user_guide/cards/subsurface/characteristic_curves_card.html

https://www.documentation.pflotran.org/qa_tests/pc_sat_rel_perm.html#modified-kosugi