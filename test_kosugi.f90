program test_kosugi

 use mod_kosugi
 implicit none

 integer, parameter :: DP = 8, N = 300
 integer :: soil
 
 real(8), parameter :: PI = 4.0_DP*atan(1.0_DP)
 
 type(sat_func_mK_type) :: sat_func
 type(rpf_mK_liq_type) :: rpf_liq
 type(rpf_mK_gas_type) :: rpf_gas
 type(option_type) :: dummy

 real(DP) :: liq_sat, dsat_dpres, cap_pres
 real(DP) :: rel_perm_liq, rel_perm_gas, dkr_dsat_liq, dkr_dsat_gas
 real(DP) :: cp_vec(N), ls_vec(N), theta(N)
 integer :: i
 
 ! test capillary pressure as a function of saturation

 soil = 1

 select case(soil)
 case(1)
   ! Hygiene Sandstone
   sat_func%sr =    0.153_DP ! -
   sat_func%sigz =  0.337_DP ! units?
   sat_func%muz =  -6.30_DP  ! units?
   sat_func%rmax =  2.52D-3  ! m
   sat_func%r0 =    1.07D-4  ! m
 case(2)     
   ! Silt Loam G.E. 3
   sat_func%sr =    0.192_DP ! -
   sat_func%sigz =  1.12_DP ! units?
   sat_func%muz =  -7.93_DP  ! units?
   sat_func%rmax =  1.27D-2  ! m
   sat_func%r0 =    1.48D-4  ! m
 case(3)
   ! Beit Netofa Clay
   sat_func%sr =    0.1_DP ! -
   sat_func%sigz =  2.33_DP ! units?
   sat_func%muz =  -11.1_DP  ! units?
   sat_func%rmax =  1.32D-2  ! m
   sat_func%r0 =    4.36D-7  ! m
 end select
   
 sat_func%pcmax = 1.0D+30  ! Pa
 
 rpf_liq%sr =   sat_func%sr
 rpf_liq%sigz = sat_func%sigz

 rpf_gas%sr =   sat_func%sr
 rpf_gas%srg = 0.001_DP
 rpf_gas%sigz = sat_func%sigz

 forall (i=1:N)
   theta(i) = PI*i/N -PI/2.0_DP
 end forall
 
 forall (i=1:N)
   ! saturation between 0 and 1
   ls_vec(i) = sat_func%sr + (1.0_DP-sat_func%sr)*(sin(theta(i)) + 1.0_DP)/2.0_DP
   !ls_vec(i) = real(i-1,DP)/real(N-1,DP)
 end forall

 open(22,file='3-parameter.out')
 sat_func%nparam = 3
 
 print *, '3 parameter'
 write(22,*) '# liquid_sat         cap_press     liq_sat_est     dsat_dpres '//&
      &'    rel_perm_liq    rel_perm_gas    dkr_dsat_liq    dkr_dsat_gas'
 write(22,*) '#',sat_func
 do i = 1,N
   call SF_mK_CapillaryPressure(sat_func, ls_vec(i), cp_vec(i), dummy)
   call SF_mK_Saturation(sat_func, cp_vec(i), liq_sat, dsat_dpres, dummy)
   call RPF_mK_Liq_RelPerm(rpf_liq, ls_vec(i), rel_perm_liq, dkr_dsat_liq, dummy)
   call RPF_mK_Gas_RelPerm(rpf_gas, ls_vec(i), rel_perm_gas, dkr_dsat_gas, dummy)
   !print *, i,ls_vec(i),cp_vec(i),liq_sat,dsat_dpres,&
   !     &rel_perm_liq,rel_perm_gas,dkr_dsat_liq,dkr_dsat_gas
   write(22,'(9(ES15.7E3,1X))') ls_vec(i),cp_vec(i),liq_sat,dsat_dpres,&
        &rel_perm_liq,rel_perm_gas,dkr_dsat_liq,dkr_dsat_gas
 end do
 print *, ''
 close(22)

 print *, 'unit test: 3-param'
 cap_pres = 1.0D+4
 print *, 'capillary pressure (IN):',cap_pres
 call SF_mK_Saturation(sat_func, cap_pres, liq_sat, dsat_dpres, dummy)
 print *, 'saturation (OUT):',liq_sat
 call RPF_mK_Liq_RelPerm(rpf_liq, liq_sat, rel_perm_liq, dkr_dsat_liq, dummy)
 print *, 'relative liq perm (OUT):',rel_perm_liq
 print *, 'dsat_dpres (OUT):',dsat_dpres
 print *, 'dkr_dsat*dsat_pres (OUT):',dkr_dsat_liq*dsat_dpres
 print *, '---------------------'
 liq_sat = 2.5D-1
 print *, 'liquid saturation (IN):',liq_sat
 call SF_mK_CapillaryPressure(sat_func, liq_sat, cap_pres, dummy)
 print *, 'capillary pressure (OUT):',cap_pres
 print *, '---------------------'
 liq_sat = 5.0D-1
 print *, 'liquid saturation (IN):',liq_sat
 call RPF_mK_Liq_RelPerm(rpf_liq, liq_sat, rel_perm_liq, dkr_dsat_liq, dummy)
 print *, 'relative liq perm (OUT):',rel_perm_liq
 print *, 'dkr_dsat_liq (OUT):',dkr_dsat_liq
 call RPF_mK_Gas_RelPerm(rpf_gas, liq_sat, rel_perm_gas, dkr_dsat_gas, dummy)
 print *, 'relative gas perm (OUT):',rel_perm_gas
 print *, 'dkr_dsat_gas (OUT):',dkr_dsat_gas
 
 open(22,file='4-parameter.out')
 sat_func%nparam = 4

 print *, '4 parameter'
 write(22,*) '# liquid_sat         cap_press     liq_sat_est     dsat_dpres '//&
      &'    rel_perm_liq    rel_perm_gas    dkr_dsat_liq    dkr_dsat_gas' 
 write(22,*) '#',sat_func
 do i = 1,N
   call SF_mK_CapillaryPressure(sat_func, ls_vec(i), cp_vec(i), dummy)
   call SF_mK_Saturation(sat_func, cp_vec(i), liq_sat, dsat_dpres, dummy)
   call RPF_mK_Liq_RelPerm(rpf_liq, ls_vec(i), rel_perm_liq, dkr_dsat_liq, dummy)
   call RPF_mK_Gas_RelPerm(rpf_gas, ls_vec(i), rel_perm_gas, dkr_dsat_gas, dummy)
   !print *, i,ls_vec(i),cp_vec(i),liq_sat,dsat_dpres,&
   !     &rel_perm_liq,rel_perm_gas,dkr_dsat_liq,dkr_dsat_gas
   write(22,'(9(ES14.7,1X))') ls_vec(i),cp_vec(i),liq_sat,dsat_dpres,&
        &rel_perm_liq,rel_perm_gas,dkr_dsat_liq,dkr_dsat_gas
 end do
 close(22)

 print *, ''
 print *, 'unit test: 4-param'
 cap_pres = 1.0D+4
 print *, 'capillary pressure (IN):',cap_pres
 call SF_mK_Saturation(sat_func, cap_pres, liq_sat, dsat_dpres, dummy)
 print *, 'saturation (OUT):',liq_sat
 call RPF_mK_Liq_RelPerm(rpf_liq, liq_sat, rel_perm_liq, dkr_dsat_liq, dummy)
 print *, 'relative liq perm (OUT):',rel_perm_liq
 print *, 'dsat_dpres (OUT):',dsat_dpres
 print *, 'dkr_dsat*dsat_dpres (OUT):',dkr_dsat_liq*dsat_dpres
  print *, '---------------------'
 liq_sat = 2.5D-1
 print *, 'liquid saturation (IN):',liq_sat
 call SF_mK_CapillaryPressure(sat_func, liq_sat, cap_pres, dummy)
 print *, 'capillary pressure (OUT):',cap_pres
 print *, '---------------------'
 liq_sat = 5.0D-1
 print *, 'liquid saturation (IN):',liq_sat
 call RPF_mK_Liq_RelPerm(rpf_liq, liq_sat, rel_perm_liq, dkr_dsat_liq, dummy)
 print *, 'relative liq perm (OUT):',rel_perm_liq
 print *, 'dkr_dsat_liq (OUT):',dkr_dsat_liq
 call RPF_mK_Gas_RelPerm(rpf_gas, liq_sat, rel_perm_gas, dkr_dsat_gas, dummy)
 print *, 'relative gas perm (OUT):',rel_perm_gas
 print *, 'dkr_dsat_gas (OUT):',dkr_dsat_gas

end program test_kosugi

  
