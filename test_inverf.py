import mpmath as mp

mp.dps = 15

n = mp.mpf(19)

# want: inverse complementary error function
# have: Q^{-1} (incorrectly called inverseErf in PFLOTRAN). Or what is that actually?

def erfcinv(x):
    # define inverse of complimentary error function through root finding
    return mp.findroot(lambda t: mp.erfc(t)-x,0.0)

if 0:
    for i in range(n):
        x = mp.mpf(i+1)/(n+1)
        # Q^{-1} is trivially related to inverse error function
        print i+1,x,mp.erfinv(2*x-1)
        # need to relate to inver complementary error function

if 1:
    for i in range(n):
        # 0 < x < 2
        x = mp.mpf(2)*mp.mpf(i+1)/(n+1)
        print i+1,x,erfcinv(x)

