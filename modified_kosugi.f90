module mod_kosugi

  implicit none
  private

  integer, parameter :: DP = 8
  
  type, public :: sat_func_mK_type
    real(DP) :: sr
    real(DP) :: pcmax ! Pa
    real(DP) :: sigz, muz
    real(DP) :: rmax, r0  ! meters
    integer :: nparam ! 3 -> no rmax
  end type sat_func_mK_type

  type, public :: rpf_mK_liq_type
    real(DP) :: sr 
    real(DP) :: sigz
  end type rpf_mK_liq_type

  type, public :: rpf_mK_gas_type
    real(DP) :: sr, srg
    real(DP) :: sigz
  end type rpf_mK_gas_type
  
  type, public :: option_type
    ! dummy -- not used in testing
    integer :: nothing
  end type option_type 

  public  SF_mK_CapillaryPressure, SF_mK_Saturation, &
       & RPF_mK_Liq_RelPerm, RPF_mK_Gas_RelPerm
  
contains
  
subroutine SF_mK_CapillaryPressure(this,liquid_saturation, &
                                   capillary_pressure,option)
  ! 
  ! Computes the capillary_pressure as a function of saturation
  !
  ! Author: Kris Kuhlman
  ! Date: 2016
  !
  !KLK!use Option_module

  use inverfc, only : InverseErf
  
  implicit none

  integer, parameter :: DP = 8

  real(DP), parameter :: KAPPA = 0.149_DP
  real(DP), parameter :: LNKAP = log(KAPPA)
  real(DP), parameter :: UNIT_CONVERSION = 9.982D+2*9.81d0/1.0D+2
  
  class(sat_func_mK_type) :: this
  real(DP), intent(in) :: liquid_saturation
  real(DP), intent(out) :: capillary_pressure
  type(option_type), intent(inout) :: option
  
  real(DP) :: Se
  real(DP) :: inverse
  real(DP) :: exparg
  real(DP) :: mueta
  real(DP) :: hc, hmaxinv
  
  
  if (liquid_saturation <= this%Sr) then
    capillary_pressure = this%pcmax
    return
  else if (liquid_saturation >= 1.d0) then
    capillary_pressure = 0.d0
    return
  endif

  Se = (liquid_saturation - this%Sr)/(1.d0 - this%Sr)
  inverse = -InverseErf(Se)  
  mueta = LNKAP - this%muz
  exparg = this%sigz*inverse + mueta
  
  hc = KAPPA/this%rmax
  capillary_pressure = exp(exparg) + hc
  if (this%nparam == 4) then
    hmaxinv = this%r0/KAPPA
    capillary_pressure = 1.d0/(1.d0/capillary_pressure + hmaxinv)
  end if
    
  capillary_pressure = min(capillary_pressure*UNIT_CONVERSION,this%pcmax)
  
end subroutine SF_mK_CapillaryPressure

! ************************************************************************** !

subroutine SF_mK_Saturation(this,capillary_pressure,liquid_saturation, &
                            dsat_dpres,option)
  ! 
  ! Computes the saturation (and associated derivatives) as a function of 
  ! capillary pressure
  ! 
  ! Author: Kris Kuhlman
  ! Date: 2016
  !
  !KLK! use Option_module
  !KLK! use Utility_module
  use inverfc, only : InverseErf
  
  implicit none

  intrinsic :: erfc ! gnu & intel extension and required in f2008

  integer, parameter :: DP = 8
  
  real(DP), parameter :: KAPPA = 0.149_DP 
  real(DP), parameter :: LNKAP = log(KAPPA)
  real(DP), parameter :: SQRT2 = sqrt(2.0_DP)
  real(DP), parameter :: SQRTPI = sqrt(4.0_DP*atan(1.0_DP))
  real(DP), parameter :: UNIT_CONVERSION = 9.982D+2*9.81d0/1.0D+2
  
  class(sat_func_mK_type) :: this
  real(DP), intent(in) :: capillary_pressure
  real(DP), intent(out) :: liquid_saturation
  real(DP), intent(out) :: dsat_dpres
  type(option_type), intent(inout) :: option
  
  real(DP) :: hc, hmax, cap_press_scaled
  real(DP) :: rt2sz
  real(DP) :: lnArg, erfcArg
  
  dsat_dpres = 0.d0 ! UNINITIALIZED_DOUBLE
  cap_press_scaled = capillary_pressure/UNIT_CONVERSION
  
  hc = KAPPA/this%rmax
  if (cap_press_scaled <= hc) then
    liquid_saturation = 1.d0
    return
  end if
  if (this%nparam == 3) then
    lnArg = cap_press_scaled - hc
  elseif (this%nparam == 4) then
    hmax = KAPPA/this%r0
    if (cap_press_scaled >= hmax) then 
      liquid_saturation = 0.d0 
      return
    end if
    lnArg = 1.d0/(1.d0/cap_press_scaled - 1.d0/hmax) - hc
  else
    print *, 'ERROR: invalid nparam (3 or 4 only): ',this%nparam
    stop
  end if
  rt2sz = SQRT2*this%sigz
  erfcArg = (log(lnArg) - LNKAP + this%muz)/rt2sz
  liquid_saturation = this%Sr + (1.0d0-this%Sr)*5.0D-1*erfc(erfcArg)
  dsat_dpres = exp(-erfcArg**2)/(SQRTPI*rt2sz*lnArg)/UNIT_CONVERSION
  
end subroutine SF_mK_Saturation
! End SF: modified Kosugi

subroutine RPF_mK_Liq_RelPerm(this,liquid_saturation, &
                              relative_permeability,dkr_sat,option)
  ! 
  ! Computes the relative permeability (and associated derivatives) as a 
  ! function of saturation
  ! 
  ! Author: Kris Kuhlman
  ! Date: 2016
  ! 
  !KLK!use Option_module
  !KLK!use Utility_module

  use inverfc, only : InverseErf
  
  implicit none

  intrinsic :: erfc
  
  integer, parameter :: DP = 8
  real(DP), parameter :: SQRT2 = sqrt(2.0_DP)
  
  class(rpf_mK_liq_type) :: this
  real(DP), intent(in) :: liquid_saturation
  real(DP), intent(out) :: relative_permeability
  real(DP), intent(out) :: dkr_sat
  type(option_type), intent(inout) :: option
  
  real(DP) :: Se, InvSatRange
  real(DP) :: erfcArg, erfcRes, dkr_Se
  real(DP) :: invErfcRes
  real(DP) :: sqrtSe, expArg

  relative_permeability = 0.d0
  dkr_sat = 0.d0 ! UNINITIALIZED_DOUBLE

  InvSatRange = 1.d0/(1.0D+0 - this%Sr)
  Se = (liquid_saturation - this%Sr)*InvSatRange
  if (Se >= 1.d0) then
    relative_permeability = 1.d0
    return
  else if (Se <= 0.d0) then
    relative_permeability = 0.d0
    return
  endif

  invErfcRes = InverseErf(Se)
  erfcArg = (this%sigz - invErfcRes)/SQRT2
  erfcRes = erfc(erfcArg)
  sqrtSe = sqrt(Se)
  relative_permeability = sqrtSe*erfcRes*5.0D-1

  ! from Wolfram Alpha (x -> Se)
  ! (InverseErfc[x] -> -1/Sqrt[x] InverseNorm[x/2])
  !
  ! D[(Sqrt[x] Erfc[sigmaz/Sqrt[2] + InverseErfc[2 x]])/2, x] =
  ! E^(InverseErfc[2 x]^2 - (simgaz/Sqrt[2] + InverseErfc[2 x])^2) * ...
  ! Sqrt[x] + Erfc[sigmaz/Sqrt[2] + InverseErfc[2 x]]/(4 Sqrt[x])
  expArg = 5.0D-1*invErfcRes**2 - erfcArg**2
  dkr_Se = erfcres/(4.0D0*sqrtSe) + sqrtSe*exp(expArg)

  ! InvSatRange = dSe/dsat
  dkr_sat = dkr_Se * InvSatRange
  
end subroutine RPF_mK_Liq_RelPerm
! End RPF: modified Kosugi (Liquid)

subroutine RPF_mK_Gas_RelPerm(this,liquid_saturation, &
                              relative_permeability,dkr_sat,option)
  ! 
  ! Computes the relative permeability (and associated derivatives) as a 
  ! function of saturation
  !    
  ! Author: Kris Kuhlman
  ! Date: 2016
  ! 
  !KLK! use Option_module

  use inverfc, only : InverseErf
  
  implicit none

  integer, parameter :: DP = 8
  real(DP), parameter :: SQRT2 = sqrt(2.0_DP)

  class(rpf_mK_gas_type) :: this
  real(DP), intent(in) :: liquid_saturation
  real(DP), intent(out) :: relative_permeability
  real(DP), intent(out) :: dkr_sat
  type(option_type), intent(inout) :: option
  
  real(DP) :: Se, Seg, InvSatRange
  real(DP) :: dkr_Se, dSe_sat
  real(DP) :: erfcArg, erfcRes
  real(DP) :: invErfcRes
  real(DP) :: sqrtSe, expArg

  InvSatRange = 1.d0/ (1.d0 - this%Sr - this%Srg)
  Se = (liquid_saturation - this%Sr)*InvSatRange
  
  relative_permeability = 0.d0
  dkr_sat = 0.d0 ! UNINITIALIZED_DOUBLE
  if (Se >= 1.d0) then
    relative_permeability = 0.d0
    return
  else if (Se <=  0.d0) then
    relative_permeability = 1.d0
    return
  endif
  
  Seg = 1.d0 - Se

  invErfcRes = InverseErf(Seg)
  erfcArg = (this%sigz - invErfcRes)/SQRT2
  erfcRes = erfc(erfcArg)
  sqrtSe = sqrt(Seg)
  relative_permeability = sqrtSe*erfcRes*5.0D-1

  ! from Wolfram Alpha (x -> Seg)
  ! (InverseErfc[x] -> -1/Sqrt[x] InverseNorm[x/2])
  !
  ! D[(Sqrt[x] Erfc[sigmaz/Sqrt[2] + InverseErfc[2 x]])/2, x] =
  ! E^(InverseErfc[2 x]^2 - (simgaz/Sqrt[2] + InverseErfc[2 x])^2) * ...
  ! Sqrt[x] + Erfc[sigmaz/Sqrt[2] + InverseErfc[2 x]]/(4 Sqrt[x])
  expArg = 5.0D-1*invErfcRes**2 - erfcArg**2
  dkr_Se = erfcres/(4.0D0*sqrtSe) + sqrtSe*exp(expArg)

  ! -1 = dSeg/dSe
  ! InvSatRange = dSe/dsat
  dkr_sat = -1.d0 * dkr_Se * InvSatRange
  
end subroutine RPF_MK_Gas_RelPerm
! End RPF: modified Kosigi (Gas)

end module mod_kosugi

