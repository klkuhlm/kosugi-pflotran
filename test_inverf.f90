program test
  use inverf, only : InverseErf ! input required: 0 <= x <= 1

  implicit none

  integer, parameter :: DP = 8, N = 19
  real(8), parameter :: SQRT2 = sqrt(2.0_DP)
  integer :: i
  real(DP) :: x

  do i=1,N
    ! 0 < x < 2
    x = 2.0_DP*real(i,DP)/real(N+1,DP)
    print *, i,x,-InverseErf(x/2.0_DP)/SQRT2
  end do

end program test
