import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from h5py import File
import operator

vg = File('pflotran_hygiene_richards_vg.h5','r')
mk3 = File('pflotran_hygiene_richards_mk3.h5','r')
mk4 = File('pflotran_hygiene_richards_mk4.h5','r')

models = [vg,mk3,mk4]

# 1st key is Coordinates
# 2nd key is Provenance
# 3rd key to end is solution at a time

# just get the label (first item in tuple)
time_str = [x for x in models[0].keys() if  'Time:' in x]

# convert labels to times for plotting
tvec = [float(x.rstrip('d').lstrip('Time:').strip()) for x in time_str]

# need numerically sorted times, not lexically (alphabetical) sorted
t = dict(zip(time_str,tvec))
sorted_times = sorted(t.items(), key=operator.itemgetter(1))

tvec = np.array([x[1] for x in sorted_times])

Z_faces = models[0]['Coordinates']['Z [m]'].value
Z_centers = (Z_faces[1:] + Z_faces[:-1])/2

# what we are interested in plotting
lab_vec = ["Capillary_Pressure [Pa]",
           "Liquid_Pressure [Pa]",
           "Liquid_Saturation",
           "Liquid_Mobility [1_Pa-s]"]

print 'models',models
print 'lab_vec',lab_vec
print 'time_str',len(time_str)
shape0 = models[0][time_str[0]][lab_vec[0]].shape

# 1D problem (z-only)
shape = (shape0[2],len(time_str),len(lab_vec),len(models))

# copy data into array, to make easier to plot across times
d = np.zeros(shape)

for k in range(len(models)):
    for i,lab in enumerate(lab_vec):
        for j,time in enumerate(tvec):
            key = 'Time:  %.5E d' % time
            d[:,j,i,k] = models[k][key][lab].value


fig = plt.figure(1,figsize=(15,5))
axes = []
axes.append(fig.add_subplot(141))
axes.append(fig.add_subplot(142))
axes.append(fig.add_subplot(143))
axes.append(fig.add_subplot(144))

for j,ax in enumerate(axes):
    if j == 1:
        SCALE = 1.0E-6
    else:
        SCALE = 1.0
    for i,(lt,label) in enumerate(zip(['r-','g--','b:'],['vg','mk3','mk4'])):
        ax.plot(d[:,-1,j,i]*SCALE,Z_centers,lt,label=label)
    ax.set_xlabel(lab_vec[j])
    if j == 0:
        ax.set_ylabel('Z [m]')
    elif j == 1:
        ax.legend(loc=0,fontsize='small')
        ax.set_xlabel(lab_vec[j].replace('[Pa]','[MPa]'))
                 
plt.tight_layout()
plt.savefig('mrc_comparison.png')


