import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import modified_kosugi as mk

UNIT_CONVERSION = 9.982E+2*9.81/1.0E+2

SIGMAZ = 3.36E-1
MUZ = -6.25
RMAX = 3.05E-3
R0 = 1.07E-5
SR = 1.53E-1

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# columns: 0saturation, 1capillary_pressure
fn = 'hygiene_mk3_sat_pc.dat'
d3 = np.loadtxt(fn,skiprows=1)

fn = 'hygiene_mk4_sat_pc.dat'
d4 = np.loadtxt(fn,skiprows=1)

fig = plt.figure(1,figsize=(12,5))
ax3 = fig.add_subplot(121)
ax4 = fig.add_subplot(122)

da3 = np.zeros_like(d3)
da3[:,1] = d3[:,1]/UNIT_CONVERSION

for j in range(da3.shape[0]):
    da3[j,0] = SR + (1-SR)*mk.MRC3(da3[j,1],SIGMAZ,MUZ,RMAX)
    
ax3.plot(d3[:,0],d3[:,1],'b.',label='PFLOTRAN')
ax3.plot(da3[:,0],d3[:,1],'r-',label='mpmath')
ax3.set_ylim(ymax=2.5E+4)
ax3.set_xlabel('$S_l$')
ax3.set_ylabel('$p_c$ [Pa]')
ax3.legend(loc=0)
ax3.set_title('MODIFIED_KOSUGI NPARAM=3\n$\\sigma_Z=.336$, $\\mu_Z=-6.25$, $R_{MAX}=.00305$')

da4 = np.zeros_like(d4)
da4[:,1] = d4[:,1]/UNIT_CONVERSION
RMAX = 2.52E-3

for j in range(da4.shape[0]):
    da4[j,0] = SR + (1-SR)*mk.MRC4(da4[j,1],SIGMAZ,MUZ,RMAX,R0)

ax4.plot(d4[:,0],d4[:,1],'b.',label='PFLOTRAN')
ax4.plot(da4[:,0],d4[:,1],'r-',label='mpmath')
ax4.set_ylim(ymax=2.5E+4)
ax4.set_xlabel('$S_l$')
ax4.set_ylabel('$p_c$ [Pa]')
ax4.legend(loc=0)
ax4.set_title('MODIFIED_KOSUGI NPARAM=4\n$\\sigma_Z=.336$, $\\mu_Z=-6.25$, $R_{MAX}=.00252$, $R_0=1.07\\times 10^{-5}$')

plt.tight_layout()
plt.savefig('hygiene_mk3_mk4_sat_pc.png')
plt.close(1)

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# columns: 0capillary_pressure, 1saturation, 2dsat/dpres, 3dsat/dpres_numerical
fn = 'hygiene_mk3_pc_sat.dat'
d3 = np.loadtxt(fn,skiprows=1)

fn = 'hygiene_mk4_pc_sat.dat'
d4 = np.loadtxt(fn,skiprows=1)

fig = plt.figure(1,figsize=(12,5))
ax3 = fig.add_subplot(121)
ax4 = fig.add_subplot(122)

ax3.semilogx(d3[:,0],d3[:,1],'r-')
ax3a = ax3.twinx()
ax3a.loglog(d3[:,0],d3[:,3],'b.',label='deriv numerical')
ax3a.loglog(d3[:,0],d3[:,2],'g-',label='deriv analytical')
ax3.set_xlabel('$p_c$ [Pa]')
ax3.set_ylabel('$S_l$',color='red')
ax3.set_ylim([0,1.05])
ax3a.set_ylim(ymin=1.0E-70)
ax3a.set_ylabel('$\\partial S_l/\\partial p_c$',color='green')
ax3a.legend(loc=0,fontsize='small')
ax3.set_title('MODIFIED_KOSUGI NPARAM=3\n$\\sigma_Z=.336$, $\\mu_Z=-6.25$, $R_{MAX}=.00305$')

ax4.semilogx(d4[:,0],d4[:,1],'r-')
ax4a = ax4.twinx()
ax4a.loglog(d4[:,0],d4[:,3],'b.',label='deriv numerical')
ax4a.loglog(d4[:,0],d4[:,2],'g-',label='deriv analytical')
ax4.set_xlabel('$p_c$ [Pa]')
ax4.set_ylabel('$S_l$',color='red')
ax4.set_ylim([0,1.05])
ax4a.set_ylim(ymin=1.0E-70)
ax4a.set_ylabel('$\\partial S_l/\\partial p_c$',color='green')
ax4a.legend(loc=0,fontsize='small')
ax4.set_title('MODIFIED_KOSUGI NPARAM=4\n$\\sigma_Z=.336$, $\\mu_Z=-6.25$, $R_{MAX}=.00252$, $R_0=1.07\\times 10^{-5}$')

plt.tight_layout()
plt.savefig('hygiene_mk3_mk4_pc_sat.png')
plt.close(1)

# $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

# columns: 0saturation, 1kr, 2dkr/dsat, 3dkr/dsat_numerical
fn = 'hygiene_mk3_liquid_rel_perm.dat'
d3l = np.loadtxt(fn,skiprows=1)

fn = 'hygiene_mk4_liquid_rel_perm.dat'
d4l = np.loadtxt(fn,skiprows=1)

fn = 'hygiene_mk3_gas_rel_perm.dat'
d3g = np.loadtxt(fn,skiprows=1)

fn = 'hygiene_mk4_gas_rel_perm.dat'
d4g = np.loadtxt(fn,skiprows=1)

fig = plt.figure(1,figsize=(12,5))
ax3 = fig.add_subplot(121)
ax4 = fig.add_subplot(122)

ax3.semilogy(d3l[:,0],d3l[:,1],'r-',label='PFLOTRAN $k_r^l$')
ax3.semilogy(d3g[:,0],d3g[:,1],'b-',label='PFLOTRAN $k_r^g$')
ax3a = ax3.twinx()
ax3a.plot(d3l[:,0],d3l[:,3],'r.',label='liquid deriv numerical')
ax3a.plot(d3l[:,0],d3l[:,2],'m-',label='liquid deriv analytical')
ax3a.plot(d3g[:,0],d3g[:,3],'b.',label='gas deriv numerical')
ax3a.plot(d3g[:,0],d3g[:,2],'c-',label='gas deriv analytical')

ax3.set_xlabel('$S_l$')
ax3.set_ylabel('$k_r$ (red=liquid, blue=gas)')
#ax3.set_ylim([0,1.05])
#ax3a.set_ylim(ymin=1.0E-70)
ax3a.set_ylabel('$\\partial k_r/\\partial S_l$')
ax3a.legend(loc=8,fontsize='x-small')
ax3.set_title('MODIFIED_KOSUGI NPARAM=3\n$\\sigma_Z=.336$, $\\mu_Z=-6.25$, $R_{MAX}=.00305$')

ax4.semilogy(d4l[:,0],d4l[:,1],'r-',label='PFLOTRAN $k_r^l$')
ax4.semilogy(d4g[:,0],d4g[:,1],'b-',label='PFLOTRAN $k_r^g$')
ax4a = ax4.twinx()
ax4a.plot(d4l[:,0],d4l[:,3],'r.',label='liquid deriv numerical')
ax4a.plot(d4l[:,0],d4l[:,2],'m-',label='liquid deriv analytical')
ax4a.plot(d4g[:,0],d4g[:,3],'b.',label='gas deriv numerical')
ax4a.plot(d4g[:,0],d4g[:,2],'c-',label='gas deriv analytical')

ax4.set_xlabel('$S_l$')
ax4.set_ylabel('$k_r$ (red=liquid, blue=gas)')
#ax4.set_ylim([0,1.05])
#ax4a.set_ylim(ymin=1.0E-70)
ax4a.set_ylabel('$\\partial k_r/\\partial S_l$')
ax4a.legend(loc=8,fontsize='x-small')
ax4.set_title('MODIFIED_KOSUGI NPARAM=4\n$\\sigma_Z=.336$, $\\mu_Z=-6.25$, $R_{MAX}=.00252$, $R_0=1.07\\times 10^{-5}$')

plt.tight_layout()
plt.savefig('hygiene_mk3_mk4_rel_perm.png')
plt.close(1)

